# grafana-gnocchi-openstack-token-renewer

A systemd service file that will regenerate an openstack token to be inserted into where it is needed.

# Deployment

1. Copy the service file to `/etc/systemd/system/`
2. Edit the ExecStart line to point to the correct openstack credentials file (`admin-openrc.sh`)
3. Set the variable `TOKEXP` to a numerical value as you probably don't have access to your providers keystone.conf
4. Edit the ExecStart line to reference the correct grafana gnocchi connector (`where name=\\"gnocchi\\"`)
5. Run `systemctl daemon-reload` and then start the service


Code has been taken from the proprietary repository here https://gitlab.flux.utah.edu/johnsond/openstack-build-ubuntu/commit/b1f221cf5186a5b270d490ff6243ef58e6cf3f5c
 and relicenced with permission from the author.